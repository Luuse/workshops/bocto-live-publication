function removeActives(){

	var activesPost = document.querySelectorAll(".post.active");

	if(activesPost.length > 0){

		activesPost.forEach((post) => {

			post.classList.remove("active");

		});

	}

}

function isNotAPart(tagstring, tag){

	var tagList = tagstring.split(",");

	return (tagList.indexOf(tag) !== -1) ? true : false;	

}



function selectProjects(actives){

	removeActives();

	if(actives.length > 0){

		actives.forEach((active) => {

			var toBeOns = document.querySelectorAll(".post[data-tags*='"+active.dataset.tag+"']");

			toBeOns.forEach((toBeOn) => {

				if(isNotAPart(toBeOn.dataset.tags, active.dataset.tag) && !toBeOn.classList.contains("active")){

					toBeOn.classList.add("active");
				}

			});

		});

	}

}

function toggleBody(actives){

	if(actives.length > 0){

		if(!document.body.classList.contains("tagMode")){

			document.body.classList.add("tagMode");

		}

	}else{

		document.body.classList.remove("tagMode");

	}

}

function toggleTag(tag){

	if(tag.classList.contains("active")){

		tag.classList.remove("active");

	}else{

		tag.classList.add("active");	
	}
}

function setActiveTags(){

	var tags = document.querySelectorAll("#tags>li");

	tags.forEach((tag) => {

		tag.addEventListener("click", () => {

			toggleTag(tag);

			var actives = document.querySelectorAll("#tags>li.active");

			toggleBody(actives);
			selectProjects(actives);

		});

	});
}

function onTagClickShow(){

	var title = document.querySelector("#tags h5");

	title.addEventListener("click", () => {

		var wrap = title.parentElement;

		if(wrap.classList.contains("visible")){

			wrap.classList.remove("visible");

		}else{

			wrap.classList.add("visible");
		}

	});
}

function byebyeBr(){

	if(window.innerWidth < 600){

		var titles = document.querySelectorAll(".title");

		titles.forEach((title) => {

			var inside = title.innerHTML.split("<br>").join("");

			title.innerHTML=inside;

		});

	}
}


document.addEventListener("DOMContentLoaded", () => {

	setActiveTags();
	onTagClickShow();
	byebyeBr();

});