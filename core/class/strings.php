<?php

class Strings{

	public function decode_utf($str) {

		$str =  urldecode(trim(str_replace("=", "", preg_replace("/\=([A-F][A-F0-9])/", "%$1", $str))));		
		$encoding = mb_detect_encoding($str, 'utf-8, iso-8859-1, ascii', true);
		
		return (strcasecmp($encoding, 'utf-8') !== 0) ? str_replace("92", "’", iconv($encoding, 'utf-8', $str)) : $str;
	}

	public function clean($string) {

		$string = str_replace(' ', '-', $string);
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); 
		
	}

	public function hasTags($text){

		return (strpos($text, "#") > -1) ? true : false;
	}

	public function getTags($text){

		$firstTags = explode("#", $text);
		$tags = [];

		foreach($firstTags as $key => $firstTag){

			if($key !== 0){

				$pos = strpos($firstTag, " ");

				if($pos > 0){

					$tags [] = $this->decode_utf(substr($firstTag, 0, $pos));

				}elseif(strpos($text, $firstTag) + strlen($firstTag) == strlen($text)){

					$tags [] = $this->decode_utf($firstTag);
				}

			}
		}

		return $tags;
	}

}