<?php

class Data extends Strings{

	private $mailbox;
	private $username;
	private $password;

	public function __construct($mailbox, $username, $password){

		$this->mailbox = $mailbox;
		$this->username = $username;
		$this->password = $password;
	}

	public function fetch(){

		$imap = imap_open($this->mailbox, $this->username, $this->password);
		$emails = imap_search($imap, 'ALL');

		rsort($emails);

		$data = new stdClass;
		$data->mails = [];	
		$globalTags = [];	

		foreach ($emails as $nu){

			$num = $nu - 1;

			$data->mails[$num] = new stdClass;

			$type = (strpos(imap_fetchbody($imap ,$nu, 1), "Content-Type") > -1) ? 1.1 : strlen(imap_fetchbody($imap ,$nu, 1)) > 5000 ? 1.1 : 1;
			$message =  $this->decode_utf(trim(imap_fetchbody($imap ,$nu, $type)));
			$head = imap_headerinfo($imap, $nu);
			$structure = imap_fetchstructure($imap, $nu);

			if(!empty($message)){

				$data->mails[$num]->body = $message;

			}


			if($this->hasTags($message)){

				$data->mails[$num]->tags = $this->getTags($message);
				$globalTags = array_merge($globalTags, $data->mails[$num]->tags);
			}

			if(property_exists($structure, "parts")){

				$data->mails[$num]->images = [];

				$parts = $structure->parts;
				$key = - 1;

				foreach($parts as $i => $part){

					if($part->type == 5){

						$key ++;

						$img = imap_fetchbody($imap ,$nu, $i + 1);

						$parameters = $part->parameters;

						foreach($parameters as $parameter){

							if($parameter->attribute == "name"){


								$dir = "imgs/".$head->udate;
								$info = pathinfo(mb_decode_mimeheader($parameter->value));
								$file = $info["filename"];
								$ext = $info["extension"];
								$name = $this->clean($file).".".$ext;
								$path = $dir."/".$name;

								if(!file_exists($path)){

									if(!file_exists($dir) || !is_dir($dir)){

										mkdir($dir);

									}

									file_put_contents($dir."/".$name, base64_decode($img));

								}

								$data->mails[$num]->images[$key] = new stdClass;

								$data->mails[$num]->images[$key]->path = $path;
								$data->mails[$num]->images[$key]->name = $name;

							}


						}


					}
				}

			}

			if(isset($head->from[0]->personal)){ $data->mails[$num]->sender =  mb_decode_mimeheader($head->from[0]->personal); }

			if($head->subject){

				$data->mails[$num]->subject = $this->decode_utf(str_replace("_", " ", mb_decode_mimeheader($head->subject)));

			}

			$data->mails[$num]->timestamp = $head->udate;

		}

		$data->tags = array_unique($globalTags);

		natcasesort($data->tags);

		file_put_contents("data/data.json", json_encode($data));
	}

	public function all(){

		return json_decode(file_get_contents("data/data.json"));
	}

}