<?php require_once("core/start.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <meta lang="fr">
    <meta charset="utf-8">
    <title>Bocto --- Live</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/main.css" title="" type="text/css">
    <link rel="stylesheet" href="assets/css/page.css" title="" type="text/css">
</head>
<body>
    <main>
     <nav>
        <div class="title" >
         B<br>O<br>C<br>T<br>O
     </div>
     <div class="title" >
         <span><br><br>L<br>I<br>V<br>E</span>
     </div>
     <ul id="tags">
        <?php foreach($data->all()->tags as $tag): ?>
            <li data-tag="<?= $tag ?>"><?= $tag ?></li>
        <?php endforeach ?> 
        <h5>Tags</h5>
    </ul>
</nav>
<section id="posts">
    <?php foreach($mails as $mail): ?>
        <div class="post" <?php if(isset($mail->tags)): ?> data-tags="<?= implode(",", $mail->tags) ?>" <?php endif ?>>
           <header>
            <?php if(isset($mail->subject)): ?>
                <h2><?= $mail->subject ?></h2>
            <?php endif ?>
            <h4><?= date("d.m.y G\hi", $mail->timestamp) ?></h4>
            <?php if(isset($mail->sender)): ?>
                <h3><?= $mail->sender ?></h3>
            <?php endif ?>
        </header>
        <?php if(isset($mail->body)): ?>
            <div class="text">
                <?= $mail->body ?>
            </div>
        <?php endif ?>    
        <?php if(isset($mail->images)): ?>
            <div class="images">
                <?php foreach($mail->images as $image): ?>
                    <img src="<?= $image->path ?>">
                <?php endforeach ?>    
            </div>
        <?php endif ?>
    </div>
<?php endforeach ?>  
</section> 
</main>
<script type="text/javascript" src="assets/js/main.js"></script>
</body>
</html>
